unit Unit9;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.Edit,
   FMX.Layouts,
   FMX.ListBox,
   FMX.ListView.Types,
   FMX.ListView.Appearances,
   FMX.ListView.Adapters.Base,
   FMX.ListView,
   FMX.TabControl;

type
   TForm9 = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      Layout1: TLayout;
      Label2: TLabel;
      Edit1: TEdit;
      Layout2: TLayout;
      ListView1: TListView;
      TabControl1: TTabControl;
      TabItem1: TTabItem;
      TabItem2: TTabItem;
      Layout3: TLayout;
      Label3: TLabel;
      Edit2: TEdit;
      Layout4: TLayout;
      Label4: TLabel;
      Edit3: TEdit;
      Layout5: TLayout;
      Button2: TButton;
      Button1: TButton;
      Button3: TButton;
      procedure FormCreate(Sender: TObject);
      procedure Edit1KeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
      procedure Edit2KeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
      procedure Button2Click(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      procedure UsersList;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form9: TForm9;

const
   FILTER_CHAR = 'QWERTYUIOPASDFGHJKLZXCVBNM ����������������.';
   FILTER_EMAIL = 'qwertyuiopasdfghjklzxcvbnm@_-.1234567890';

implementation

uses
   uDM;

{$R *.fmx}

procedure TForm9.UsersList;
var
   LItem: TListViewItem;
begin
   DM.FDQuery1.Close;
   DM.FDQuery1.Open;

   if not DM.FDQuery1.IsEmpty then
   begin
      DM.FDQuery1.First;
      ListView1.Items.Clear;

      while not DM.FDQuery1.Eof do
      begin
         LItem := ListView1.Items.Add;
         LItem.Tag := DM.FDQuery1.FindField('ID_USER').AsInteger;
         LItem.Text := DM.FDQuery1.FindField('DS_NAME').AsString;
         LItem.Detail := DM.FDQuery1.FindField('DS_EMAIL').AsString;

         DM.FDQuery1.Next;
      end;
   end;
end;

procedure TForm9.FormCreate(Sender: TObject);
begin
   Edit1.FilterChar := FILTER_CHAR;
   Edit2.FilterChar := FILTER_EMAIL;
end;

procedure TForm9.FormShow(Sender: TObject);
begin
   UsersList;
end;

procedure TForm9.Edit1KeyDown(Sender: TObject; //
   var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
   if (Key = vkReturn) then
      Edit2.SetFocus;
end;

procedure TForm9.Edit2KeyDown(Sender: TObject; //
   var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
   if (Key = vkReturn) then
      Edit3.SetFocus;
end;

procedure TForm9.Button2Click(Sender: TObject);
var
   LName: string;
   LEmail: string;
   LPassword: string;

   LSQL: string;
const
   SQL_INS = //
      'INSERT INTO TB_USER (' + sLineBreak + //
      'DS_NAME,DS_EMAIL,DS_PASSWORD' + sLineBreak + //
      ') VALUES (%s,%s,%s)';
begin
   LName := QuotedStr(Edit1.Text);
   LEmail := QuotedStr(Edit2.Text);
   LPassword := QuotedStr(Edit3.Text);

   LSQL := Format(SQL_INS, [LName, LEmail, LPassword]);
   DM.FDConnection1.ExecSQL(LSQL);

   UsersList;

   TabControl1.ActiveTab := TabItem1;
end;

end.
