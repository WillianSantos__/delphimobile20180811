unit Unit4;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.Calendar,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.Layouts;

type
   TForm4 = class(TForm)
      Layout1: TLayout;
      Label2: TLabel;
      ToolBar1: TToolBar;
      Label1: TLabel;
      Calendar1: TCalendar;
      Button1: TButton;
      procedure Button1Click(Sender: TObject);
      procedure Calendar1DateSelected(Sender: TObject);
   private
      procedure CaptureDate;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form4: TForm4;

implementation

{$R *.fmx}

procedure TForm4.Button1Click(Sender: TObject);
begin
   CaptureDate;
end;

procedure TForm4.Calendar1DateSelected(Sender: TObject);
begin
   CaptureDate;
end;

procedure TForm4.CaptureDate;
begin
   ShowMessage(DateToStr(Calendar1.Date));
end;

end.
