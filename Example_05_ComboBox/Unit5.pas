unit Unit5;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.Layouts,
   FMX.Edit,
   FMX.ListBox;

type
   TForm5 = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      Button1: TButton;
      Layout1: TLayout;
      Label2: TLabel;
      Edit1: TEdit;
      ComboBox1: TComboBox;
      Layout2: TLayout;
      Label3: TLabel;
      procedure FormShow(Sender: TObject);
      procedure Button1Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form5: TForm5;

implementation

{$R *.fmx}

procedure TForm5.Button1Click(Sender: TObject);
begin
   if not(Edit1.Text = EmptyStr) then
   begin
      ComboBox1.Items.Add(Edit1.Text);
      Edit1.Text := EmptyStr;
   end;
   Edit1.SetFocus;
end;

procedure TForm5.FormShow(Sender: TObject);
begin
   ComboBox1.Items.Clear;
end;

end.
