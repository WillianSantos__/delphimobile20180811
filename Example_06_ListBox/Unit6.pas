unit Unit6;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Edit,
   FMX.Controls.Presentation,
   FMX.Layouts,
   FMX.ListBox;

type
   TForm6 = class(TForm)
      Layout1: TLayout;
      Label2: TLabel;
      Edit1: TEdit;
      Layout2: TLayout;
      Label3: TLabel;
      ToolBar1: TToolBar;
      Label1: TLabel;
      Button1: TButton;
      ListBox1: TListBox;
      procedure Button1Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form6: TForm6;

implementation

{$R *.fmx}

procedure TForm6.Button1Click(Sender: TObject);
begin
   if not(Edit1.Text = EmptyStr) then
   begin
      ListBox1.Items.Add(Edit1.Text);
      Edit1.Text := EmptyStr;
   end;
   Edit1.SetFocus;
end;

end.
