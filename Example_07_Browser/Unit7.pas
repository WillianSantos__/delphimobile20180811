unit Unit7;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.WebBrowser,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.Edit;

type
   TForm7 = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      WebBrowser1: TWebBrowser;
      Button1: TButton;
      Edit1: TEdit;
      procedure Edit1KeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
      procedure Button1Click(Sender: TObject);
      procedure WebBrowser1DidFinishLoad(ASender: TObject);
      procedure FormShow(Sender: TObject);
   private
      procedure OpenUrl(AUrl: string);
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form7: TForm7;

implementation

{$R *.fmx}

procedure TForm7.OpenUrl(AUrl: string);
begin
   if not(Edit1.Text = EmptyStr) then
      WebBrowser1.Navigate(Edit1.Text)
   else
      Edit1.SetFocus;
end;

procedure TForm7.Edit1KeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
   if (Key = vkReturn) then
   begin
      OpenUrl(Edit1.Text);
   end;
   Button1.SetFocus;
end;

procedure TForm7.FormShow(Sender: TObject);
begin
   OpenUrl(Edit1.Text);
end;

procedure TForm7.Button1Click(Sender: TObject);
begin
   WebBrowser1.GoBack;
end;

procedure TForm7.WebBrowser1DidFinishLoad(ASender: TObject);
begin
   Edit1.Text := WebBrowser1.URL;
end;

end.
