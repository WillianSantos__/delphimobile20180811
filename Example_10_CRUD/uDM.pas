unit uDM;

interface

uses
   {$IFDEF MSWINDOWS}
   FMX.Forms,
   {$ENDIF}
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.UI,
   FireDAC.DApt,
   FireDAC.DApt.Intf,
   FireDAC.DatS,
   FireDAC.FMXUI.Wait,
   FireDAC.Phys,
   FireDAC.Phys.Intf,
   FireDAC.Phys.SQLite,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.Async,
   FireDAC.Stan.Def,
   FireDAC.Stan.Error,
   FireDAC.Stan.ExprFuncs,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Param,
   FireDAC.Stan.Pool,
   FireDAC.UI.Intf,
   System.Classes,
   System.SysUtils;

type
   TDM = class(TDataModule)
      FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
      FDGUIxWaitCursor1: TFDGUIxWaitCursor;
      FDConnection1: TFDConnection;
      FDQuery1: TFDQuery;
      FDTable1: TFDTable;
    FDQuery2: TFDQuery;
      procedure FDConnection1BeforeConnect(Sender: TObject);
      procedure DataModuleCreate(Sender: TObject);
      procedure FDConnection1AfterConnect(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   DM: TDM;

const
   SQLite_File = 'Example_10_Database.sdb';

   SQLite_Users = //
      'CREATE TABLE IF NOT EXISTS TB_USER (' + sLineBreak + //
      'ID_USER INTEGER PRIMARY KEY AUTOINCREMENT,' + sLineBreak + //
      'DS_NAME VARCHAR(255),' + sLineBreak + //
      'DS_EMAIL VARCHAR(255),' + sLineBreak + //
      'DS_PASSWORD VARCHAR(255),' + sLineBreak + //
      'IM_USER VARCHAR(32768),' + sLineBreak + //
      'FL_STATUS INTEGER DEFAULT 1,' + sLineBreak + //
      'DH_INS DATETIME DEFAULT CURRENT_TIMESTAMP,' + sLineBreak + //
      'DH_UPD DATETIME,' + sLineBreak + //
      'DH_DEL DATETIME' + sLineBreak + //
      ')';

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses
   System.IOUtils;

{$R *.dfm}

procedure TDM.FDConnection1AfterConnect(Sender: TObject);
begin
   FDConnection1.ExecSQL(SQLite_Users);
end;

procedure TDM.FDConnection1BeforeConnect(Sender: TObject);
var
   LFile: string;
begin
   {$IF DEFINED(iOS) or DEFINED(ANDROID)}
   LFile := TPath.Combine(TPath.GetDocumentsPath, SQLite_File);
   {$ELSE}
   {$IFDEF MSWINDOWS}
   LFile := ExtractFilePath(ParamStr(0)) + SQLite_File;
   {$ENDIF}
   {$ENDIF}
   {$IFDEF MSWINDOWS}
   if not FileExists(LFile) then
      Application.Terminate;
   {$ENDIF}
   FDConnection1.Params.Values['Database'] := LFile;
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
   FDQuery1.Close;
   FDQuery1.Open;
end;

end.
