program Example_10_CRUD;

uses
   System.StartUpCopy,
   FMX.Forms,
   Unit10 in 'Unit10.pas' {Form10} ,
   uDM in 'uDM.pas' {DM: TDataModule};

{$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TForm10, Form10);
   Application.CreateForm(TDM, DM);
   Application.Run;

end.
