unit Unit10;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.Edit,
   FMX.Layouts,
   FMX.ListBox,
   FMX.ListView.Types,
   FMX.ListView.Appearances,
   FMX.ListView.Adapters.Base,
   FMX.ListView,
   FMX.TabControl,
   FMX.Objects,
   FMX.Platform,
   FMX.DialogService;

type
   TForm10 = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      Layout1: TLayout;
      Label2: TLabel;
      Edit1: TEdit;
      Layout2: TLayout;
      ListView1: TListView;
      TabControl1: TTabControl;
      TabItem1: TTabItem;
      TabItem2: TTabItem;
      Layout3: TLayout;
      Label3: TLabel;
      Edit2: TEdit;
      Layout4: TLayout;
      Label4: TLabel;
      Edit3: TEdit;
      Layout5: TLayout;
      Button2: TButton;
      Button3: TButton;
      Button1: TButton;
      Image1: TImage;
      Image2: TImage;
      Image3: TImage;
      Image4: TImage;
      Button4: TButton;
      procedure FormCreate(Sender: TObject);
      procedure Edit1KeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
      procedure Edit2KeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
      procedure Button2Click(Sender: TObject);
      procedure FormShow(Sender: TObject);
      procedure Button3Click(Sender: TObject);
      procedure ListView1ItemClick(const Sender: TObject; const AItem: TListViewItem);
      procedure Button1Click(Sender: TObject);
      procedure ListView1PullRefresh(Sender: TObject);
      procedure Button4Click(Sender: TObject);
   private
      FCrudState: Integer; // 0 - Normal | 1 - Insert | 2 - Update | 3 - Delete
      FUserId: Integer;
      procedure UsersList;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form10: TForm10;

const
   FILTER_CHAR = 'QWERTYUIOPASDFGHJKLZXCVBNM ����������������.';
   FILTER_EMAIL = 'qwertyuiopasdfghjklzxcvbnm@_-.1234567890';

implementation

uses
   uDM;

{$R *.fmx}

procedure TForm10.UsersList;
var
   LItem: TListViewItem;
   intRandom: Integer;
begin
   DM.FDQuery1.Close;
   DM.FDQuery1.Open;

   if not DM.FDQuery1.IsEmpty then
   begin
      DM.FDQuery1.First;
      ListView1.Items.Clear;

      while not DM.FDQuery1.Eof do
      begin
         LItem := ListView1.Items.Add;
         LItem.Tag := DM.FDQuery1.FindField('ID_USER').AsInteger;
         LItem.Text := DM.FDQuery1.FindField('DS_NAME').AsString;
         LItem.Detail := DM.FDQuery1.FindField('DS_EMAIL').AsString;

         intRandom := Random(3);

         case intRandom of
            1:
               begin
                  LItem.Bitmap.Assign(Image1.Bitmap);
               end;
            2:
               begin
                  LItem.Bitmap.Assign(Image2.Bitmap);
               end;
            3:
               begin
                  LItem.Bitmap.Assign(Image3.Bitmap);
               end;
            4:
               begin
                  LItem.Bitmap.Assign(Image4.Bitmap);
               end;
         end;

         DM.FDQuery1.Next;
      end;
   end;
end;

procedure TForm10.FormCreate(Sender: TObject);
begin
   FCrudState := 0;

   Edit1.FilterChar := FILTER_CHAR;
   Edit2.FilterChar := FILTER_EMAIL;

   TabControl1.ActiveTab := TabItem1;

   Button1.Visible := False;
   Button2.Visible := False;
   Button4.Visible := False;

   TabItem2.Visible := False;

   Image1.Visible := False;
   Image2.Visible := False;
   Image3.Visible := False;
   Image4.Visible := False;

   ListView1.AlternatingColors := True;
   ListView1.NativeOptions := [TListViewNativeOption.Grouped, TListViewNativeOption.Indexed, TListViewNativeOption.Styled];
   ListView1.PullRefreshWait := True;
   ListView1.PullToRefresh := True;
   ListView1.SearchVisible := True;
end;

procedure TForm10.FormShow(Sender: TObject);
begin
   UsersList;
end;

procedure TForm10.ListView1ItemClick(const Sender: TObject; const AItem: TListViewItem);
var
   LName: string;
   LEmail: string;

   LSQL: string;
const
   SQL_SEL = //
      'SELECT DS_PASSWORD FROM TB_USER WHERE ID_USER = %s';
begin
   FCrudState := 2;

   FUserId := AItem.Tag;

   LName := AItem.Text;
   LEmail := AItem.Detail;

   // ShowMessage( //
   // 'INFO' + sLineBreak + sLineBreak + //
   // 'Id: ' + IntToStr(LUserId) + sLineBreak + //
   // 'Name: ' + LName + sLineBreak + //
   // 'Email: ' + LEmail //
   // );

   LSQL := Format(SQL_SEL, [IntToStr(FUserId)]);
   DM.FDQuery2.Open(LSQL);

   Edit3.Text := DM.FDQuery2.Fields[0].AsString;
   Edit2.Text := LEmail;
   Edit1.Text := LName;

   TabControl1.ActiveTab := TabItem2;
   TabItem2.Visible := True;

   TabItem1.Visible := False;
   Button3.Visible := False;

   Button4.Visible := True;
   Button2.Visible := True;
   Button1.Visible := True;
end;

procedure TForm10.ListView1PullRefresh(Sender: TObject);
begin
   UsersList;
end;

procedure TForm10.Edit1KeyDown(Sender: TObject; //
   var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
   if (Key = vkReturn) then
      Edit2.SetFocus;
end;

procedure TForm10.Edit2KeyDown(Sender: TObject; //
   var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
   if (Key = vkReturn) then
      Edit3.SetFocus;
end;

procedure TForm10.Button3Click(Sender: TObject);
begin
   FCrudState := 1;

   TabItem1.Visible := False;

   {$IF DEFINED(iOS) or DEFINED(ANDROID)}
   Button2.StyleLookup := 'donetoolbutton';
   {$ELSE}
   {$IFDEF MSWINDOWS}
   Button2.Text := 'Save';
   {$ENDIF}
   {$ENDIF}
   Button3.Visible := False;
   Button2.Visible := True;
   Button4.Visible := True;
   Button1.Visible := True;

   TabControl1.ActiveTab := TabItem2;
   TabItem2.Visible := True;

   Edit1.Text := EmptyStr;
   Edit2.Text := EmptyStr;
   Edit3.Text := EmptyStr;
   Edit1.SetFocus;
end;

procedure TForm10.Button4Click(Sender: TObject);
var
   LSQL: string;
const
   SQL_DEL = //
      'DELETE FROM TB_USER WHERE ID_USER = %s';
begin
   TDialogService.MessageDialog('Choose a button:', System.UITypes.TMsgDlgType.mtInformation, //
      [System.UITypes.TMsgDlgBtn.mbYes, System.UITypes.TMsgDlgBtn.mbNo, System.UITypes.TMsgDlgBtn.mbCancel], //
      System.UITypes.TMsgDlgBtn.mbYes, 0, //

      procedure(const AResult: TModalResult)
      begin
         case AResult of
            mrYES:
               begin
                  LSQL := Format(SQL_DEL, [IntToStr(FUserId)]);
                  DM.FDConnection1.ExecSQL(LSQL);

                  UsersList;

                  TabControl1.ActiveTab := TabItem1;
                  TabItem1.Visible := True;
                  TabItem2.Visible := False;
                  Button3.Visible := True;
                  Button2.Visible := False;
                  Button4.Visible := False;
                  Button1.Visible := False;
               end;

         end;
      end);

end;

procedure TForm10.Button1Click(Sender: TObject);
begin
   FCrudState := 0;
   FUserId := 0;

   TabControl1.ActiveTab := TabItem1;
   TabItem1.Visible := True;
   TabItem2.Visible := False;
   Button3.Visible := True;
   Button2.Visible := False;
   Button4.Visible := False;
   Button1.Visible := False;
end;

procedure TForm10.Button2Click(Sender: TObject);
var
   LName: string;
   LEmail: string;
   LPassword: string;

   LSQL: string;
const
   SQL_INS = //
      'INSERT INTO TB_USER (' + sLineBreak + //
      'DS_NAME,DS_EMAIL,DS_PASSWORD' + sLineBreak + //
      ') VALUES (%s,%s,%s)';

   SQL_UPD = //
      'UPDATE TB_USER SET' + sLineBreak + //
      ' DS_NAME = %s' + sLineBreak + //
      ',DS_EMAIL = %s' + sLineBreak + //
      ',DS_PASSWORD = %s' + sLineBreak + //
      'WHERE ID_USER = %s';
begin
   LName := Edit1.Text;
   LEmail := Edit2.Text;
   LPassword := Edit3.Text;

   if not(LName = EmptyStr) and //
      not(LEmail = EmptyStr) and //
      not(LPassword = EmptyStr) then
   begin
      case FCrudState of
         1:
            begin
               LSQL := Format(SQL_INS, //
                  [QuotedStr(LName), QuotedStr(LEmail), QuotedStr(LPassword)]);
               DM.FDConnection1.ExecSQL(LSQL);
            end;
         2:
            begin
               LSQL := Format(SQL_UPD, //
                  [QuotedStr(LName), QuotedStr(LEmail), QuotedStr(LPassword), //
                  IntToStr(FUserId)]);
               DM.FDConnection1.ExecSQL(LSQL);
            end;
      end;

      UsersList;

      TabControl1.ActiveTab := TabItem1;
      TabItem1.Visible := True;
      TabItem2.Visible := False;
      Button3.Visible := True;
      Button2.Visible := False;
      Button4.Visible := False;
      Button1.Visible := False;
   end
   else
   begin
      ShowMessage('Fileds Empty!');
   end;
end;

end.
