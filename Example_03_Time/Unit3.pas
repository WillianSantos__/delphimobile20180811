unit Unit3;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.DateTimeCtrls,
   FMX.Layouts;

type
   TForm3 = class(TForm)
      Layout1: TLayout;
      Label2: TLabel;
      Layout2: TLayout;
      Label3: TLabel;
      Layout3: TLayout;
      Label4: TLabel;
      Layout4: TLayout;
      Label5: TLabel;
      ToolBar1: TToolBar;
      Label1: TLabel;
      TimeEdit1: TTimeEdit;
      TimeEdit2: TTimeEdit;
      TimeEdit3: TTimeEdit;
      TimeEdit4: TTimeEdit;
      Timer1: TTimer;
      procedure Timer1Timer(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form3: TForm3;

implementation

{$R *.fmx}

procedure TForm3.FormShow(Sender: TObject);
begin
   Self.Timer1.Enabled := True;
end;

procedure TForm3.Timer1Timer(Sender: TObject);
begin
   Self.TimeEdit2.Time := Now;
end;

end.
