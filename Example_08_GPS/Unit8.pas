unit Unit8;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   System.Sensors,
   System.Sensors.Components,
   FMX.WebBrowser,
   FMX.TabControl;

type
   TForm8 = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      LocationSensor1: TLocationSensor;
      wbGM: TWebBrowser;
      Switch1: TSwitch;
      Label2: TLabel;
      tbcMaps: TTabControl;
      tbiGM: TTabItem;
      tbiOSM: TTabItem;
      wbOSM: TWebBrowser;
      procedure Switch1Switch(Sender: TObject);
      procedure LocationSensor1LocationChanged(Sender: TObject; const OldLocation, NewLocation: TLocationCoord2D);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form8: TForm8;

const
   mapOSM = 'https://www.openstreetmap.org/#map=17/%s/%s';
   mapGM = 'http://maps.google.com/maps?q=%s,%s';
   LatLng = 'Latitude: %s | Longitude: %s';

implementation

{$R *.fmx}

procedure TForm8.Switch1Switch(Sender: TObject);
begin
   tbcMaps.ActiveTab := tbiGM;
   LocationSensor1.Active := Switch1.Enabled;
end;

procedure TForm8.LocationSensor1LocationChanged(Sender: TObject; //
   const OldLocation, NewLocation: TLocationCoord2D);
var
   LLat: string;
   LLng: string;
begin
   LLat := FormatFloat('0.000000', NewLocation.Latitude);
   LLng := FormatFloat('0.000000', NewLocation.Longitude);

   LLat := StringReplace(LLat, ',', '.', [rfReplaceAll]);
   LLng := StringReplace(LLng, ',', '.', [rfReplaceAll]);

   Label2.Text := Format(LatLng, [LLat, LLng]);

   wbGM.Navigate(Format(mapGM, [LLat, LLng]));
   wbOSM.Navigate(Format(mapOSM, [LLat, LLng]));
end;

end.
