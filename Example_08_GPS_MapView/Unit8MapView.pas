unit Unit8MapView;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   System.Sensors,
   System.Sensors.Components,
   FMX.WebBrowser,
   FMX.TabControl,
   FMX.Maps;

type
   TForm8MapView = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      LocationSensor1: TLocationSensor;
      wbGM: TWebBrowser;
      Switch1: TSwitch;
      Label2: TLabel;
      tbcMaps: TTabControl;
      tbiGM: TTabItem;
      tbiOSM: TTabItem;
      wbOSM: TWebBrowser;
      tbiNative: TTabItem;
      MapView1: TMapView;
      procedure Switch1Switch(Sender: TObject);
      procedure LocationSensor1LocationChanged(Sender: TObject; const OldLocation, NewLocation: TLocationCoord2D);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form8MapView: TForm8MapView;

const
   mapOSM = 'https://www.openstreetmap.org/#map=17/%s/%s';
   mapGM = 'http://maps.google.com/maps?q=%s,%s';
   LatLng = 'Latitude: %s | Longitude: %s';

implementation

{$R *.fmx}

procedure TForm8MapView.Switch1Switch(Sender: TObject);
begin
   tbcMaps.ActiveTab := tbiNative;
   LocationSensor1.Active := Switch1.Enabled;
end;

procedure TForm8MapView.LocationSensor1LocationChanged(Sender: TObject; //
   const OldLocation, NewLocation: TLocationCoord2D);
var
   LLat: string;
   LLng: string;
   mapCenter: TMapCoordinate;
begin
   LLat := FormatFloat('0.000000', NewLocation.Latitude);
   LLng := FormatFloat('0.000000', NewLocation.Longitude);

   LLat := StringReplace(LLat, ',', '.', [rfReplaceAll]);
   LLng := StringReplace(LLng, ',', '.', [rfReplaceAll]);

   Label2.Text := Format(LatLng, [LLat, LLng]);

   MapView1.Location.Create(StrToFloat(LLat), StrToFloat(LLng));

   mapCenter := TMapCoordinate.Create(StrToFloat(LLat), StrToFloat(LLng));
   MapView1.Location := mapCenter;

   wbGM.Navigate(Format(mapGM, [LLat, LLng]));
   wbOSM.Navigate(Format(mapOSM, [LLat, LLng]));
end;

end.
